# maia-specification

Specifications for music representations that are used in various applications by [Music Artificial Intelligence Algorithms, Inc.](http://musicintelligence.co/)

## Installation, usage, and contributing

This repository contains specifications in JSON format, so there is no need/point for installation/usage guidance. See links in subsequent sections (e.g., under Composition object) for interfaces where it is possible to experiment with the representations.

We **encourage** use of the representations specified here (within the terms of the GNU General Public License), and visitors are **welcome to post feature requests and issues** to the issue tracker.

There are many other music representations out there. Those in this repository are motivated by wanting representations that (1) are in JSON format, (2) support collaboration in and evolution of musical activity, (3) bridge the audio-symbolic gap, and (4) promote research in music computing.

## Composition object

Encodes when sounds will be triggered and how they will appear visually.

Fairly traditional approach to the representation of a song or piece of music. Can be rendered conveniently as piano roll or sheet music. For examples, see [here](http://jam.musicintelligence.co/#!/editor/if_ye_love_me) for piano roll (playback rendered using [Tone.js](https://github.com/Tonejs/Tone.js), Firefox recommended) and [here](http://crunchy.musicintelligence.co/composition/) for the corresponding sheet music (notation rendered using [VexFlow](http://www.vexflow.com/)).

## Instrument object

Encodes what sounds will be triggered. We have not finished the specification for the Instrument object yet, although it is used already, e.g. in the [Jam!](http://jam.musicintelligence.co/) interface.

## Etc.

Other representations will be published here in due course. We know we are overlooking many ways of making and interacting with music right now (audio routing, visual programming, mixing, DJ'ing to name a few).

## See also

* [Music Encoding Initiative](http://music-encoding.org/)
* [W3C Music Notation Community Group](https://www.w3.org/community/music-notation/)
* [LilyBin](http://lilybin.com/)
* [LilyPond](http://lilypond.org/)
* [LilyPond Score extension](https://www.mediawiki.org/wiki/Extension:Score)
* kern at Chapters 2 and 6 [here](http://humdrum.ccarh.org/)
* [MusicXML](https://www.musicxml.com/)

## Release history

* 0.0.0 Initial release

## Keywords

music representations